var arrNum = [];
function getNumber() {
  if (document.querySelector("#inputNum").value.trim() == "") {
    return;
  }
  var number = document.querySelector("#inputNum").value * 1;

  arrNum.push(number);
  //    show kết quả

  document.querySelector("#txtArray").innerHTML =
    /*html*/
    `<div>
    <p> Mảng hiện tại: ${arrNum} </p>
    </div>`;
}
// bài 1
function showTongSoChan() {
  document.querySelector("#txtSum").innerHTML =
    /*html*/
    `<div>
  <p>Tổng số dương: ${tinhTongSoDuong(arrNum)}</p>
  </div>`;
}
// bài 2
function showDemSoDuong() {
  document.querySelector("#txtCount").innerHTML =
    /*html*/
    `<div>
    <p>Tổng số chẵn: ${demSoLuongSoDuong(arrNum)}</p>
    </div>`;
}
// bài 3
function showSoNhoNhat() {
  document.querySelector("#txtMin").innerHTML =
    /*html*/
    `<div>
      <p>Số nhỏ nhất: ${timSoNhoNhat(arrNum)}</p>
      </div>`;
}
// bài 4
function timSoDuongNhoNhat() {
  for (var n = [], r = 0; r < arrNum.length; r++)
    arrNum[r] > 0 && n.push(arrNum[r]);
  if (n.length > 0) {
    for (var e = n[0], r = 1; r < n.length; r++) n[r] < e && (e = n[r]);
    document.getElementById("txtMinPos").innerHTML = "Số dương nhỏ nhất: " + e;
  } else
    document.getElementById("txtMinPos").innerHTML =
      "Không có số dương trong mảng";
}
// bài 5
function timSoChanCuoiCung() {
  for (var n = 0, r = 0; r < arrNum.length; r++)
    arrNum[r] % 2 == 0 && (n = arrNum[r]);
  document.getElementById("txtEven").innerHTML = "Số chẵn cuối cùng: " + n;
}
// bài 6
function doiCho() {
  swap(
    document.getElementById("inputIndex1").value,
    document.getElementById("inputIndex2").value
  ),
    (document.getElementById("txtChangePos").innerHTML =
      "Mảng sau khi đổi: " + arrNum);
}
// bài 7
function sapXepTangDan() {
  for (var n = 0; n < a.length; n++)
    for (var r = 0; r < arrNum.length - 1; r++)
      arrNum[r] > arrNum[r + 1] && swap(r, r + 1);
  document.getElementById("txtIncrease").innerHTML =
    "Mảng sau khi sắp xếp: " + arrNum;
}
// bài 8
function timSoNT() {
  for (var n = -1, r = 0; r < arrNum.length; r++) {
    if (checkPrime(arrNum[r])) {
      n = arrNum[r];
      break;
    }
  }
  document.getElementById("txtPrime").innerHTML = n;
}
// bài 9
function timSoNguyen() {
  for (var i = 0, n = 0; i < arrNum.length; i++) {
    if (fits(arrNum[i]) == true) {
      n++;
    }
  }
  document.getElementById(
    "txtInt"
  ).innerHTML = ` Số nguyên có trong mảng: ${n} `;
}

// bài 10
function soSanhSoAmDuong() {
  for (var n = 0, r = 0, e = 0; e < arrNum.length; e++)
    arrNum[e] > 0 ? n++ : arrNum[e] < 0 && r++;
  document.getElementById("txtCompare").innerHTML =
    n > r
      ? "Số dương > Số âm"
      : n < r
      ? "Số âm > Số dương"
      : "Số âm = Số dương";
}
